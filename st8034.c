// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (c) 2019, Procenne Corporation
 */

#include <linux/circ_buf.h>
#include <linux/fs.h>
#include <linux/gpio/consumer.h>
#include <linux/kernel.h>
#include <linux/miscdevice.h>
#include <linux/module.h>
#include <linux/mutex.h>
#include <linux/of.h>
#include <linux/of_platform.h>
#include <linux/serdev.h>
#include <linux/slab.h>

#define DRV_NAME "st8034"
#define DRV_VERSION "0.1.0"

#define CIRC_ADD(idx, size, value)	(((idx) + (value)) & ((size) - 1))

enum {
	// Maximum APDU size (bytes)
	MAX_APDU_SIZE	= 512,
};

enum st8034_flags {
	SF_DEVICE_OPENED,
	SF_ATR_RECEIVED,
	SF_ATR_IS_VALID,
	SF_PPS_REPLY_RECEIVED,
	SF_PPS_REPLY_IS_VALID,
	SF_PB_RECEIVED,
	SF_PB_IS_VALID,
	SF_WAITING_CMD_REPLY,
	SF_DATA_XFER,
	SF_ONE_BYTE_XFER,
};

struct st8034 {
	struct serdev_device *serdev;
	struct miscdevice miscdev;

	u32 clk;
	struct gpio_descs *vcc_sel_gpios;
	struct gpio_desc *cmdvcc_gpio;
	struct gpio_desc *off_gpio;
	struct gpio_desc *rstin_gpio;

	struct mutex rx_lock;

	struct circ_buf rx_buf;
	size_t rx_bytes_to_skip;

	char atr[33];
	size_t atr_size;
	char ta1;
	// Command byte in last command header
	char cmd;

	unsigned long flags;
	wait_queue_head_t msg_wait;
	wait_queue_head_t io_wait;
};

static int st8034_debug;
module_param_named(debug, st8034_debug, int, 0600);
MODULE_PARM_DESC(debug, "Enable debug output.");

static inline struct st8034 *serdev_to_st8034(struct serdev_device *serdev)
{
	return serdev_device_get_drvdata(serdev);
}

static inline struct st8034 *miscdev_to_st8034(struct miscdevice *miscdev)
{
	return container_of(miscdev, struct st8034, miscdev);
}

static unsigned int st8034_get_fi(struct st8034 *st8034)
{
	switch (st8034->ta1 & 0xF0) {
	case 0x00:
	case 0x10:
	case 0x70:
	case 0x80:
		return 372;
	case 0x20:
		return 558;
	case 0x30:
		return 744;
	case 0x40:
		return 1116;
	case 0x50:
		return 1488;
	case 0x60:
		return 1860;
	case 0x90:
		return 512;
	case 0xA0:
		return 768;
	case 0xB0:
		return 1024;
	case 0xC0:
		return 1536;
	case 0xD0:
		return 2048;
	default:
		break;
	}

	return 372;
}

static unsigned int st8034_get_di(struct st8034 *st8034)
{
	switch (st8034->ta1 & 0x0F) {
	case 0x00:
	case 0x01:
	case 0x07:
		return 1;
	case 0x02:
		return 2;
	case 0x03:
		return 4;
	case 0x04:
		return 8;
	case 0x05:
		return 16;
	case 0x06:
		return 32;
	case 0x08:
		return 12;
	case 0x09:
		return 20;
	default:
		break;
	}

	return 1;
}

static void st8034_set_baudrate(struct st8034 *st8034)
{
	unsigned int speed;

	speed = st8034_get_di(st8034) * st8034->clk / st8034_get_fi(st8034);
	speed = serdev_device_set_baudrate(st8034->serdev, speed);

	if (st8034_debug)
		dev_info(&st8034->serdev->dev, "Using baudrate: %u\n", speed);
}

static int st8034_send(struct st8034 *st8034, const unsigned char *buf,
		       size_t count)
{
	struct circ_buf *cb = &st8034->rx_buf;
	int ret;

	ret = mutex_lock_interruptible(&st8034->rx_lock);
	if (ret < 0)
		return ret;

	cb->head = cb->tail = 0;

	if (st8034_debug)
		print_hex_dump_bytes(KBUILD_MODNAME ": SND: ",
				     DUMP_PREFIX_OFFSET, buf, count);

	ret = serdev_device_write_buf(st8034->serdev, buf, count);
	if (ret >= 0)
		st8034->rx_bytes_to_skip = ret;

	mutex_unlock(&st8034->rx_lock);
	return ret;
}

static void st8034_parse_atr(struct st8034 *st8034)
{
	struct circ_buf *cb = &st8034->rx_buf;
	char *p = &st8034->atr[0];
	ssize_t len;
	unsigned int Yi, K;
	int i, j;
	bool t1_support = false;

	clear_bit(SF_ATR_IS_VALID, &st8034->flags);

	len = min_t(size_t, sizeof(st8034->atr),
		    CIRC_CNT(cb->head, cb->tail, MAX_APDU_SIZE));
	if (len < 2)
		return;

	if (cb->tail < cb->head) {
		memcpy(p, &cb->buf[cb->tail], len);
	} else {
		size_t hlen =
			CIRC_CNT_TO_END(cb->head, cb->tail, MAX_APDU_SIZE);
		memcpy(p, &cb->buf[cb->tail], hlen);
		memcpy(p + hlen, cb->buf, len - hlen);
	}

	if (*p++ != 0x3B)
		goto out;

	Yi = (*p >> 4) & 0x0F;
	K = *p & 0x0F;
	p++;
	len -= 2 + K;

	for (i = 1;; i++) {
		for (j = 0; j < 4; j++)
			if (Yi & BIT(j))
				len--;

		if (len < 0)
			return;

		if (Yi & 0x1) {
			if (i == 1)
				st8034->ta1 = *p;

			p++;
		}

		if (Yi & 0x2)
			p++;

		if (Yi & 0x4)
			p++;

		if (Yi & 0x8) {
			if (*p & 0xF)
				t1_support = true;
			Yi = (*p++ >> 4) & 0x0F;
		} else {
			break;
		}
	}

	st8034->atr_size = p - st8034->atr + K;

	if (t1_support) {
		// If T1 is supported, a checksum byte is expected.
		char tck = 0;

		st8034->atr_size++;
		for (i = 1; i < st8034->atr_size; i++)
			tck ^= st8034->atr[i];

		if (tck)
			// Invalid checksum
			goto out;
	}

	set_bit(SF_ATR_IS_VALID, &st8034->flags);

out:
	cb->head = cb->tail = 0;
	set_bit(SF_ATR_RECEIVED, &st8034->flags);
	wake_up_interruptible(&st8034->msg_wait);
}

static void st8034_parse_pps(struct st8034 *st8034)
{
	struct circ_buf *cb = &st8034->rx_buf;
	char pbuf[8];
	unsigned int opt_byte_flags;
	int num_opt_bytes;
	char pps1 = 0x11;
	char pck;
	char *p = &pbuf[0];
	size_t len;
	int i;

	clear_bit(SF_PPS_REPLY_IS_VALID, &st8034->flags);

	len = min_t(size_t, sizeof(pbuf),
		    CIRC_CNT(cb->head, cb->tail, MAX_APDU_SIZE));
	if (len < 3)
		return;

	if (cb->tail < cb->head) {
		memcpy(p, &cb->buf[cb->tail], len);
	} else {
		size_t hlen =
			CIRC_CNT_TO_END(cb->head, cb->tail, MAX_APDU_SIZE);
		memcpy(p, &cb->buf[cb->tail], hlen);
		memcpy(p + hlen, cb->buf, len - hlen);
	}

	if (*p++ != 0xFF)
		goto out;

	opt_byte_flags = (*p++ >> 4) & 0x0F;
	num_opt_bytes = 0;

	for (i = 0; i < 3; i++)
		if (opt_byte_flags & BIT(i))
			num_opt_bytes++;

	if (len < 2 + num_opt_bytes + 1)
		return;

	if (opt_byte_flags & 0x1)
		pps1 = *p++;

	if (opt_byte_flags & 0x2)
		p++;

	if (opt_byte_flags & 0x4)
		p++;

	p = &pbuf[0];
	len = 2 + num_opt_bytes + 1;
	pck = 0;

	for (i = 0; i < len; i++)
		pck ^= p[i];

	if (pck)
		goto out;

	st8034->ta1 = pps1;
	set_bit(SF_PPS_REPLY_IS_VALID, &st8034->flags);

out:
	cb->head = cb->tail = 0;
	set_bit(SF_PPS_REPLY_RECEIVED, &st8034->flags);
	wake_up_interruptible(&st8034->msg_wait);
}

static void st8034_get_pb(struct st8034 *st8034)
{
	struct circ_buf *cb = &st8034->rx_buf;
	char pb;

	if (CIRC_CNT(cb->head, cb->tail, MAX_APDU_SIZE) < 1)
		return;

	pb = cb->buf[cb->tail];

	if (pb == st8034->cmd || ~pb == st8034->cmd) {
		cb->tail = CIRC_ADD(cb->tail, MAX_APDU_SIZE, 1);
		set_bit(SF_DATA_XFER, &st8034->flags);

		if (pb == st8034->cmd)
			clear_bit(SF_ONE_BYTE_XFER, &st8034->flags);
		else
			set_bit(SF_ONE_BYTE_XFER, &st8034->flags);

	} else if (pb == 0x60 || pb == 0x61 || pb == 0x6C) {
		clear_bit(SF_DATA_XFER, &st8034->flags);

	} else {
		clear_bit(SF_PB_IS_VALID, &st8034->flags);
		goto out;
	}

	set_bit(SF_PB_IS_VALID, &st8034->flags);

out:
	set_bit(SF_PB_RECEIVED, &st8034->flags);
	wake_up_interruptible(&st8034->msg_wait);
}

static void st8034_shutdown(struct st8034 *st8034)
{
	gpiod_set_value_cansleep(st8034->cmdvcc_gpio, 0);
	usleep_range(10000, 15000);
	gpiod_set_value_cansleep(st8034->rstin_gpio, 0);
	// NOTE: Repeated open-close test fails if we do not wait enough between close-open
	usleep_range(10000, 15000);
}

static int st8034_miscdev_open(struct inode *inode, struct file *file)
{
	char pts_cmd[] = { 0xff, 0x10, 0x11, 0xfe };
	struct st8034 *st8034 = miscdev_to_st8034(file->private_data);
	int ret;

	if (test_and_set_bit(SF_DEVICE_OPENED, &st8034->flags))
		return -EBUSY;

	if (gpiod_get_value_cansleep(st8034->off_gpio)) {
		ret = -ENODEV;
		goto out_clear_bit;
	}

	if (mutex_lock_interruptible(&st8034->rx_lock)) {
		ret = -ERESTARTSYS;
		goto out_clear_bit;
	}

	st8034->rx_buf.head = 0;
	st8034->rx_buf.tail = 0;
	st8034->rx_bytes_to_skip = 0;
	st8034->ta1 = 0;
	st8034->flags = BIT(SF_DEVICE_OPENED);

	st8034_set_baudrate(st8034);

	gpiod_set_value_cansleep(st8034->cmdvcc_gpio, 1);
	usleep_range(10000, 15000);
	gpiod_set_value_cansleep(st8034->rstin_gpio, 1);

	mutex_unlock(&st8034->rx_lock);

	// wait for ATR
	ret = wait_event_interruptible_timeout(
		st8034->msg_wait, test_bit(SF_ATR_RECEIVED, &st8034->flags),
		msecs_to_jiffies(100));

	if (ret < 0) {
		ret = -ERESTARTSYS;
		goto out_shutdown;
	}

	if (!test_bit(SF_ATR_IS_VALID, &st8034->flags)) {
		ret = -ENXIO;
		goto out_shutdown;
	}

	// send PPS request
	if (st8034->ta1) {
		pts_cmd[2] = st8034->ta1;
		pts_cmd[3] = pts_cmd[0] ^ pts_cmd[1] ^ pts_cmd[2];
	}

	ret = st8034_send(st8034, pts_cmd, sizeof(pts_cmd));
	if (ret < 0)
		goto out_shutdown;

	// wait for PPS reply
	ret = wait_event_interruptible_timeout(st8034->msg_wait,
					       test_bit(SF_PPS_REPLY_RECEIVED,
							&st8034->flags),
					       msecs_to_jiffies(100));

	if (ret < 0)
		goto out_shutdown;

	if (!ret || !test_bit(SF_PPS_REPLY_IS_VALID, &st8034->flags)) {
		ret = -ENXIO;
		goto out_shutdown;
	}

	st8034_set_baudrate(st8034);
	return 0;

out_shutdown:
	st8034_shutdown(st8034);
out_clear_bit:
	clear_bit(SF_DEVICE_OPENED, &st8034->flags);
	return ret;
}

static int st8034_miscdev_release(struct inode *inode, struct file *file)
{
	struct st8034 *st8034 = miscdev_to_st8034(file->private_data);

	st8034_shutdown(st8034);
	clear_bit(SF_DEVICE_OPENED, &st8034->flags);
	return 0;
}

static ssize_t st8034_miscdev_read(struct file *file, char __user *buf,
				   size_t count, loff_t *ppos)
{
	struct st8034 *st8034 = miscdev_to_st8034(file->private_data);
	struct circ_buf *cb = &st8034->rx_buf;
	ssize_t len;
	ssize_t ret;

	if (!test_bit(SF_WAITING_CMD_REPLY, &st8034->flags)) {
		if (*ppos >= st8034->atr_size)
			return 0;

		len = min_t(ssize_t, st8034->atr_size - *ppos, count);

		if (copy_to_user(buf, st8034->atr + *ppos, len))
			return -EFAULT;

		*ppos += len;
		return len;
	}

	serdev_device_write_flush(st8034->serdev);

	if (mutex_lock_interruptible(&st8034->rx_lock))
		return -ERESTARTSYS;

	len = min_t(size_t, count,
		    CIRC_CNT_TO_END(cb->head, cb->tail, MAX_APDU_SIZE));

	if (copy_to_user(buf, &cb->buf[cb->tail], len)) {
		mutex_unlock(&st8034->rx_lock);
		return -EFAULT;
	}

	cb->tail = CIRC_ADD(cb->tail, MAX_APDU_SIZE, len);
	buf += len;
	count -= len;
	ret = len;

	if (count) {
		len = min_t(size_t, count,
			    CIRC_CNT(cb->head, cb->tail, MAX_APDU_SIZE));

		if (copy_to_user(buf, &cb->buf[cb->tail], len)) {
			mutex_unlock(&st8034->rx_lock);
			return -EFAULT;
		}

		ret += len;
	}

	mutex_unlock(&st8034->rx_lock);
	clear_bit(SF_WAITING_CMD_REPLY, &st8034->flags);
	return ret;
}

static ssize_t st8034_miscdev_write(struct file *file, const char __user *buf,
				    size_t count, loff_t *ppos)
{
	struct st8034 *st8034 = miscdev_to_st8034(file->private_data);
	char *kbuf;
	char *data;
	int ret;

	if (count < 5)
		return -EINVAL;

	kbuf = memdup_user(buf, count);
	if (IS_ERR(kbuf))
		return PTR_ERR(kbuf);

	st8034->cmd = kbuf[1];
	clear_bit(SF_PB_RECEIVED, &st8034->flags);

	ret = st8034_send(st8034, kbuf, 5);
	if (ret < 0)
		goto out_free_kbuf;

	data = kbuf + ret;
	count -= ret;

	do {
		// wait for procedure byte
		if (wait_event_interruptible_timeout(
			    st8034->msg_wait,
			    test_bit(SF_PB_RECEIVED, &st8034->flags),
			    msecs_to_jiffies(1000)) < 0) {
			ret = -ERESTARTSYS;
			goto out_free_kbuf;
		}

		if (!test_bit(SF_PB_IS_VALID, &st8034->flags))
			break;

		if (!test_bit(SF_DATA_XFER, &st8034->flags) || count == 0)
			break;

		if (test_bit(SF_ONE_BYTE_XFER, &st8034->flags)) {
			clear_bit(SF_PB_RECEIVED, &st8034->flags);
			ret = st8034_send(st8034, data, 1);
		} else {
			ret = st8034_send(st8034, data, count);
		}

		if (ret < 0)
			goto out_free_kbuf;

		data += ret;
		count -= ret;

	} while (count);

	set_bit(SF_WAITING_CMD_REPLY, &st8034->flags);
	ret = data - kbuf;

out_free_kbuf:
	kfree(kbuf);
	return ret;
}

static const struct file_operations st8034_miscdev_fops = {
	.owner		= THIS_MODULE,
	.open		= st8034_miscdev_open,
	.release	= st8034_miscdev_release,
	.read		= st8034_miscdev_read,
	.write		= st8034_miscdev_write,
	.llseek		= default_llseek,
};

static int st8034_serdev_receive(struct serdev_device *serdev,
				 const unsigned char *data, size_t count)
{
	struct st8034 *st8034 = serdev_to_st8034(serdev);
	struct circ_buf *cb = &st8034->rx_buf;
	size_t bytes_to_skip;
	size_t len;
	int ret = count;

	if (!test_bit(SF_DEVICE_OPENED, &st8034->flags))
		return ret;

	if (st8034_debug)
		print_hex_dump_bytes(KBUILD_MODNAME ": RCV: ",
				     DUMP_PREFIX_OFFSET, data, count);

	if (mutex_lock_interruptible(&st8034->rx_lock))
		return -ERESTARTSYS;

	bytes_to_skip = min(st8034->rx_bytes_to_skip, count);

	if (bytes_to_skip) {
		data += bytes_to_skip;
		count -= bytes_to_skip;
		st8034->rx_bytes_to_skip -= bytes_to_skip;
	}

	if (count > CIRC_SPACE(cb->head, cb->tail, MAX_APDU_SIZE)) {
		mutex_unlock(&st8034->rx_lock);
		return -ENOBUFS;
	}

	len = min_t(size_t, count,
		    CIRC_SPACE_TO_END(cb->head, cb->tail, MAX_APDU_SIZE));
	memcpy(&cb->buf[cb->head], data, len);
	cb->head = CIRC_ADD(cb->head, MAX_APDU_SIZE, len);
	data += len;
	count -= len;

	if (count) {
		memcpy(&cb->buf[cb->head], data, count);
		cb->head = CIRC_ADD(cb->head, MAX_APDU_SIZE, count);
	}

	if (!test_bit(SF_ATR_RECEIVED, &st8034->flags))
		st8034_parse_atr(st8034);

	else if (!test_bit(SF_PPS_REPLY_RECEIVED, &st8034->flags))
		st8034_parse_pps(st8034);

	else if (!test_bit(SF_PB_RECEIVED, &st8034->flags))
		st8034_get_pb(st8034);

	mutex_unlock(&st8034->rx_lock);
	return ret;
}

static const struct serdev_device_ops st8034_serdev_ops = {
	.receive_buf	= st8034_serdev_receive,
	.write_wakeup	= serdev_device_write_wakeup,
};

static int st8034_probe(struct serdev_device *serdev)
{
	struct device *dev = &serdev->dev;
	struct st8034 *st8034;
	int ret;

	st8034 = devm_kzalloc(dev, sizeof(*st8034), GFP_KERNEL);
	if (!st8034)
		return -ENOMEM;

	st8034->rx_buf.buf = devm_kmalloc(dev, MAX_APDU_SIZE, GFP_KERNEL);
	if (!st8034->rx_buf.buf)
		return -ENOMEM;

	st8034->serdev = serdev;
	serdev_device_set_drvdata(serdev, st8034);
	serdev_device_set_client_ops(serdev, &st8034_serdev_ops);

	if (of_property_read_u32(dev->of_node, "clock-frequency",
				 &st8034->clk)) {
		dev_err(dev, "clock-frequency not defined");
		return -EINVAL;
	}

	st8034->vcc_sel_gpios =
		devm_gpiod_get_array(dev, "vcc-sel", GPIOD_OUT_LOW);
	if (IS_ERR(st8034->vcc_sel_gpios)) {
		ret = PTR_ERR(st8034->vcc_sel_gpios);
		dev_err(dev, "Unable to get vcc-sel gpios: %d", ret);
		return ret;
	}

	if (st8034->vcc_sel_gpios->ndescs < 2) {
		dev_err(dev, "Not enough vcc-sel gpios found");
		return -EINVAL;
	}

	// VCC_SEL1	VCC_SEL2	Vcc
	// --------	--------	---
	//   LOW	   *		1.8 V
	//   HIGH	  HIGH		5 V
	//   HIGH	  LOW		3 V

	// Switch to 3 V
	gpiod_set_value_cansleep(st8034->vcc_sel_gpios->desc[0], 1);
	gpiod_set_value_cansleep(st8034->vcc_sel_gpios->desc[1], 0);

	st8034->cmdvcc_gpio = devm_gpiod_get(dev, "cmdvcc", GPIOD_OUT_LOW);
	if (IS_ERR(st8034->cmdvcc_gpio)) {
		ret = PTR_ERR(st8034->cmdvcc_gpio);
		dev_err(dev, "Unable to get cmdvcc gpio: %d", ret);
		return ret;
	}

	st8034->off_gpio = devm_gpiod_get(dev, "off", GPIOD_IN);
	if (IS_ERR(st8034->off_gpio)) {
		ret = PTR_ERR(st8034->off_gpio);
		dev_err(dev, "Unable to get off gpio: %d", ret);
		return ret;
	}

	st8034->rstin_gpio = devm_gpiod_get(dev, "rstin", GPIOD_OUT_LOW);
	if (IS_ERR(st8034->rstin_gpio)) {
		ret = PTR_ERR(st8034->rstin_gpio);
		dev_err(dev, "Unable to get rstin gpio: %d", ret);
		return ret;
	}

	ret = devm_serdev_device_open(dev, serdev);
	if (ret) {
		dev_err(dev, "Unable to open device\n");
		return ret;
	}

	serdev_device_set_flow_control(serdev, false);

	ret = serdev_device_set_parity(serdev, SERDEV_PARITY_EVEN);
	if (ret) {
		dev_err(dev, "Unable to set parity\n");
		return ret;
	}

	ret = serdev_device_set_stop_bits(serdev, 2);
	if (ret) {
		dev_err(dev, "Unable to set stop bits\n");
		return ret;
	}

	st8034->miscdev.name = DRV_NAME;
	st8034->miscdev.minor = MISC_DYNAMIC_MINOR;
	st8034->miscdev.fops = &st8034_miscdev_fops;

	ret = misc_register(&st8034->miscdev);
	if (ret)
		return ret;

	mutex_init(&st8034->rx_lock);
	init_waitqueue_head(&st8034->msg_wait);
	init_waitqueue_head(&st8034->io_wait);

	return devm_of_platform_populate(dev);
}

static void st8034_remove(struct serdev_device *serdev)
{
	struct st8034 *st8034 = serdev_device_get_drvdata(serdev);

	misc_deregister(&st8034->miscdev);
	gpiod_set_value_cansleep(st8034->vcc_sel_gpios->desc[0], 0);
	if (st8034->vcc_sel_gpios->ndescs > 1)
		gpiod_set_value_cansleep(st8034->vcc_sel_gpios->desc[1], 0);
	mutex_destroy(&st8034->rx_lock);
}

static const struct of_device_id st8034_of_match[] = {
	{ .compatible = "st,st8034" },
	{},
};
MODULE_DEVICE_TABLE(of, st8034_of_match);

static struct serdev_device_driver st8034_driver = {
	.driver	= {
		.name		= DRV_NAME,
		.of_match_table	= st8034_of_match,
	},
	.probe	= st8034_probe,
	.remove	= st8034_remove,
};

module_serdev_device_driver(st8034_driver);

MODULE_DESCRIPTION("ST8034 Smartcard Reader Driver");
MODULE_AUTHOR("Procenne Corporation");
MODULE_AUTHOR("Fatih Asici <fatih.asici@procenne.com>");
MODULE_LICENSE("GPL");
MODULE_VERSION(DRV_VERSION);
